package movies.data.paras.imdb;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;




public class MainActivity extends AppCompatActivity {
    // the predefined 4 second time for the splash screen to be displayed.
    private final int MAIN_SCREEN_TIME_OUT = 5500;
    private final int ANIM_LAYOUT_TIME_OUT = 5000;
    private final int ANIM_TEXT1_TIME_OUT = 1500;

    CardView splash_image;
    TextView txt;
    TextView txt2;
    RelativeLayout relLayoutMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this line below will remove the notification bar from the top, making the splash screen to use  full screen .
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // setting the layout of the main activity, the splash screen.
        setContentView(R.layout.activity_main);


        txt = (TextView) findViewById(R.id.theTv);
        txt.setVisibility(View.INVISIBLE);
        txt2 = (TextView) findViewById(R.id.theTv2);
        splash_image = (CardView)findViewById(R.id.splash_image);
        relLayoutMain = (RelativeLayout)findViewById(R.id.relLayoutMain);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/TitilliumWeb-Regular.ttf");
        txt.setTypeface(font);
        txt2.setTypeface(font);


        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.logo_anim);
        splash_image.startAnimation(animation);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txt.setVisibility(View.VISIBLE);
                Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.text_anim);
                txt.startAnimation(animation2);
            }
        }, ANIM_TEXT1_TIME_OUT);




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.layout_anim);
                relLayoutMain.startAnimation(animation2);
            }
        }, ANIM_LAYOUT_TIME_OUT);




        // an object of the Handler class to handle the time of the splash screen and where to go after that.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // intent to the home activity that is visible when splash screen time finishes.
                Intent intent = new Intent(MainActivity.this, MyHomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, MAIN_SCREEN_TIME_OUT);
    }

    @Override
    public void onBackPressed() {
    }
}

